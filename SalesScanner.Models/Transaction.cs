﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace SalesScanner.Models
{
    public class Transaction
    {
        public Guid id = Guid.NewGuid();
        public List<Product> productList { get; set; } = new List<Product>();
        public double saleTotal { get; set; }
    }
}
