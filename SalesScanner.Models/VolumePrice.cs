﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace SalesScanner.Models
{
    public class VolumePrice
    {
        public int quantity { get; set; }
        public double price { get; set; }
    }
}
