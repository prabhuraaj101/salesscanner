﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace SalesScanner.Models
{
    public class Product
    {
        public string name { get; set; }
        public string code { get; set; }
        public double price { get; set; }
        public VolumePrice volumePrice { get; set; }
    }
}
