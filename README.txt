XE Programming Exercise:

Requirement:
Implement a class library for a point-of-sale scanning system that accepts an arbitrary ordering of
products, similar to what would happen at an actual checkout line, then returns the correct total
price for an entire shopping cart based on per-unit or volume prices as applicable.

Solution should contain some way of running automated tests to prove that it works.


Implementation:
Implemented a Class library for PointOfSaleTerminal, no persistence , no UI provided in this application.

Price setting feature is provided as added option, if price is not set the default pricing provided in exercise document applies.


Run Test:
To run the test, move to project folder and enter from power shell

dotnet test .\SalesScanner.Tests\



