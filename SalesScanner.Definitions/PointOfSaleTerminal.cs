﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using SalesScanner.Models;

namespace SalesScanner.Definitions
{
    public class PointOfSaleTerminal : IPointOfSaleTerminal
    {
        private Transaction sale = new Transaction();
        private Dictionary<string, Product> products = ProductMenu.GetList();

        public Product SetPricing(string productCode, double unitPrice, double volumnePricing, int volume)
        {
            if (unitPrice <= 0 && volumnePricing <= 0)
                throw new InvalidOperationException("Service called without pricing information.");

            VolumePrice volumePrice = new VolumePrice();

            if (volume > 0 ^ volumnePricing > 0)
            {
                throw new InvalidOperationException("Missing values for size of volume and price for the volume.");
            }
            else if (volumnePricing > 0)
            {
                volumePrice = new VolumePrice
                {
                    price = volumnePricing,
                    quantity = volume
                };
            }

            products[productCode].price = unitPrice;
            if (volumePrice != null)
                products[productCode].volumePrice = volumePrice;

            return products[productCode];
        }

        public Transaction ScanProduct(string productCode)
        {
            if (products.ContainsKey(productCode))
            {
                sale.productList.Add(products[productCode]);
                return sale;
            }
            else
            {
                throw new KeyNotFoundException("Product code not found");
            }
        }

        public Transaction GetSaleSummary()
        {
            return sale;
        }

        public List<Product> GetUpdatedProductMenu()
        {
            return products.Select(g => g.Value).ToList();
        }

        public Double CalculateTotal()
        {
            double saleTotal = 0.0;
            Product productInCart;

            foreach (var productSold in sale.productList.GroupBy(p => p.code)){
                productInCart = products[productSold.Key];

                saleTotal += (productInCart.volumePrice == null || productSold.Count() < productInCart.volumePrice.quantity)
                    ? productInCart.price * productSold.Count()
                    : ((productSold.Count() % productInCart.volumePrice.quantity) * productInCart.price) + ((productSold.Count() / productInCart.volumePrice.quantity) * productInCart.volumePrice.price);
            }

            sale.saleTotal = saleTotal;

            return sale.saleTotal;
        }
    }
}
