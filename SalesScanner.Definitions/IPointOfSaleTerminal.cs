﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using SalesScanner.Models;

namespace SalesScanner.Definitions
{
    public interface IPointOfSaleTerminal
    {
        Product SetPricing(string productCode, double unitPrice, double volumnePricing = 0.0, int volume = 0);
        Transaction ScanProduct(string productCode);
        Double CalculateTotal();
        Transaction GetSaleSummary();
        List<Product> GetUpdatedProductMenu();
    }
}
