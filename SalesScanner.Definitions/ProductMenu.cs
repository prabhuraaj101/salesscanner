﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using SalesScanner.Models;

namespace SalesScanner.Definitions
{
    static class ProductMenu
    {
        public static Dictionary<string, Product> GetList()
        {
            return new Dictionary<string, Product>
            {
                { "A", new Product{
                    name = "Product A",
                    code = "A",
                    price = 1.25,
                    volumePrice = new VolumePrice
                    {
                        quantity = 3,
                        price = 3.00
                    }
                }},
                { "B", new Product{
                    name = "Product B",
                    code = "B",
                    price = 4.25
                }},
                { "C", new Product{
                    name = "Product C",
                    code = "C",
                    price = 1.00,
                    volumePrice = new VolumePrice
                    {
                        quantity = 6,
                        price = 5
                    }
                }},
                { "D", new Product{
                    name = "Product D",
                    code = "D",
                    price = 0.75
                }}
            };
        }
    }    
}
