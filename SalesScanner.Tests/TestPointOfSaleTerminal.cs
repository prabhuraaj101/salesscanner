﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using Xunit;
using SalesScanner.Definitions;

namespace SalesScanner.Tests
{
    public class TestPointOfSaleTerminal
    {
        [Fact]
        public void TestSetPricing()
        {
            IPointOfSaleTerminal terminal = new PointOfSaleTerminal();
            var unitPrice = 1;
            var volume = 3;
            var volumePrice = 4;
            terminal.SetPricing("A", unitPrice, volumePrice, volume);
            var updatedProduct = terminal.GetUpdatedProductMenu().Where(p => p.code == "A").Single();
            Assert.Equal(updatedProduct.price, unitPrice);
            Assert.Equal(updatedProduct.volumePrice.price, volumePrice);
            Assert.Equal(updatedProduct.volumePrice.quantity, volume);

            unitPrice = 0;
            volumePrice = 0;

            var ex = Assert.Throws<InvalidOperationException>(() => { terminal.SetPricing("B", unitPrice, volumePrice, volume); });
            var message = "Service called without pricing information.";
            Assert.Equal(ex.Message, message);

            unitPrice = 1;
            volume = 3;
            volumePrice = 0;
            ex = Assert.Throws<InvalidOperationException>(() => { terminal.SetPricing("B", unitPrice, volumePrice, volume); });
            message = "Missing values for size of volume and price for the volume.";
            Assert.Equal(ex.Message, message);
        }

        [Theory]
        [InlineData("ABCDABA", 13.25)]
        [InlineData("CCCCCCC", 6)]
        [InlineData("ABCD", 7.25)]
        [InlineData("CCCCCCCD", 6.75)]
        [InlineData("ABCDACACCC", 13)]
        public void TestCalculateTotal(string seq, double expected)
        {
            IPointOfSaleTerminal terminal = new PointOfSaleTerminal();
            var prodCodes = seq.ToCharArray();

            for (var i = 0; i < prodCodes.Length; i++)
            {
                terminal.ScanProduct(prodCodes[i].ToString());
            }

            var total = terminal.CalculateTotal();
            Assert.Equal(total, expected);
        }

        [Fact]
        public void TestCalculateTotalAfterUpdatingProduct()
        {
            IPointOfSaleTerminal terminal = new PointOfSaleTerminal();
            var unitPrice = 1;
            var volume = 3;
            var volumePrice = 4;

            terminal.SetPricing("A", unitPrice, volumePrice, volume);
            var updatedProduct = terminal.GetUpdatedProductMenu().Where(p => p.code == "A").Single();
            string[] prodArray = new string[] { "A", "B", "C", "D", "A", "B", "A" };
            var expected = 14.25;

            for (var i = 0; i < prodArray.Length; i++)
            {
                terminal.ScanProduct(prodArray[i]);
            }
            var total = terminal.CalculateTotal();

            Assert.Equal(total, expected);
        }
    }
}
